;(function ($) {

    $.fn.dragdropd = function(s){
        var self = $(this);
        $.data(self, "dragdropd-options", $.extend({
            item: 'li',
            handle: false,
            update: function() {}
        }, $.fn.dragdropd.defaults, s));

        // если сортировка не ограничена нажатием по определенному селектору внутри элемента,
        // то перемещаем за всю площадь элемента
        if ($.data(self, "dragdropd-options").handle === false) {
            $.data(self, "dragdropd-options").handle = $.data(self, "dragdropd-options").item;
        }

        // Возвращает объект с элементами контейнера и их id
        function serialize() {
            var elements = $(self).find($.data(self, "dragdropd-options").item), object={};
            $.each(elements, function(num, e){
                object[num] = $(e).attr('id');
            });
            return object;
        }

        // API
        $.extend(self, {
            serialize: serialize
        });

        // функция отмены выделения текста
        function disableSelection(){
            return false;
        }

        // нажали на элементе
        function actionClick(e) {
            var drag = $(e.currentTarget.closest($.data(self, "dragdropd-options").item));
            // верхняя граница контейнера
            var posParentTop = self.offset().top;
            // нижняя граница контейнера
            var posParentBottom = posParentTop + self.outerHeight(true);
            // координаты исходного положения элемента
            var posOld = drag.offset().top;
            // коррекция относительно позиции курсора при нажатии
            var posOldCorrection = e.pageY - posOld;
            // поднимаем нажатый элемент по z-оси
            drag.addClass('dragActive');
            // перетягиваем элемент
            var mouseMove = function(e){
                // получаем новые динамические координаты элемента
                var posNew = e.pageY - posOldCorrection;
                // если элемент перетянут выше верхней границы контейнера
                if (posNew < posParentTop){
                    // устанавливаем позицию элемента, равную позиции родителя
                    drag.offset({'top': posParentTop});
                    // меняем элемент с предыдущим в DOM, если он (предыдущий элемент) существует
                    // замещаемый элемент перемещаем плавно, с анимацией
                    if (drag.prev().length > 0 ) {
                        drag.insertBefore(drag.prev().css({'top':-drag.outerHeight(true)}).animate({'top':0}, 100));
                    }
                    drag.offset({'top': posParentTop});
                // если элемент перетянут ниже нижней границы контейнера
                } else if (posNew + drag.outerHeight(true) > posParentBottom){
                    // устанавливаем позицию элемента, равную позиции родителя + высоте родителя - высоте элемента
                    drag.offset({'top': posParentBottom - drag.outerHeight(true)});
                    // меняем элемент со следующим в DOM, если он (следующий элемент) существует
                    // замещаемый элемент перемещаем плавно, с анимацией
                    if (drag.next().length > 0 ) {
                        drag.insertAfter(drag.next().css({'top':drag.outerHeight(true)}).animate({'top':0}, 100));
                    }
                    drag.offset({'top': posParentBottom - drag.outerHeight(true)});
                // если элемент в пределах контейнера
                } else {
                    // устанавливаем новую высоту (элемент перемещается за курсором)
                    drag.offset({'top': posNew});
                    // если элемент перемещен вверх на собственную высоту
                    if (posOld - posNew > drag.outerHeight(true) - 1){
                        // меняем элемент с предыдущим в DOM
                        drag.insertBefore(drag.prev().css({'top':-drag.outerHeight(true)}).animate({'top':0}, 100));
                        // обнуляем позицию
                        drag.css({'top':0});
                        // снова получаем координаты исходного и текущего положения
                        posOld = drag.offset().top;
                        posNew = e.pageY - posOldCorrection;
                        posOldCorrection = e.pageY - posOld;
                    // если элемент перемещен вниз на собственную высоту
                    } else if (posNew - posOld > drag.outerHeight(true) - 1){
                        // меняем элемент со следующим в DOM
                        drag.insertAfter(drag.next().css({'top':drag.outerHeight(true)}).animate({'top':0}, 100));
                        drag.css({'top':0});
                        posOld = drag.offset().top;
                        posNew = e.pageY - posOldCorrection;
                        posOldCorrection = e.pageY - posOld;
                    }
                }
            };
            // отпускаем клавишу мыши
            var mouseUp = function(){
                // завершаем выполнение функции
                $(document).off('mousemove', mouseMove).off('mouseup', mouseUp);
                // отключаем функцию отмены выделения текста
                if (typeof disableSelection !== "undefined")
                    $(document).off('mousedown', disableSelection);
                // плавно возвращаем наш элемент на ближайшее освободившееся место
                drag.animate({'top':0}, 100, function(){
                    // возвращаем z-позицию на уровень остальных элементов
                    drag.removeClass('dragActive');
                });
                // здесь сохраняем новый порядок элементов
                // (cookie или post-запрос на сервер, зависит от поставленной задачи)
                $.data(self, "dragdropd-options").update(self);
            };
            // подключаем выполнение функций перемещения и отпускания клавиши мыши
            // завершаем выполнение функции, если нажата правая клавиша мыши
            $(document).on('mousemove', mouseMove).on('mouseup', mouseUp).on('contextmenu', mouseUp);
            // включаем функцию отмены выделения текста
            $(document).on('mousedown', disableSelection);
            // завершаем выполнение, если окно потеряло фокус (например, переключение на другую вкладку)
            $(window).on('blur', mouseUp);
        };

        self.on("mousedown", $.data(self, "dragdropd-options").handle, actionClick);
    };

}(jQuery));